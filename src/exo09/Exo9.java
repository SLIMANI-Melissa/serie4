package exo09;

import java.util.function.Predicate;


public class Exo9 {

	public static void main(String[] args) {

	
		Predicate<String> p1 = s -> s.length() > 4 ;
		System.out.println("cette cha�ne fait plus de 4 caract�res? " + p1.test("je"));
		
		Predicate<String> p2 = s -> !s.isEmpty();
		System.out.println("cette cha�ne est non vide ?" + p2.test("melissa"));
		
		Predicate<String> p3 = s -> s.length()==5 ;
		System.out.println("cette cha�ne fait exactement 5 caract�res de long ?" + p3.test("melis"));
		
		Predicate<String> p4 = s -> s.startsWith("J") ;
		System.out.println("cette cha�ne commence par un J " + p4.test("Jolie"));

		Predicate<String> p5= p3.and(p4); 
		System.out.println("cette cha�ne commence par un J  fait exactement 5 caract�res de long? " + p5.test("Jolie"));
	 
	}
}
