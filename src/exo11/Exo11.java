package exo11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class Exo11 {
	public static void main(String[] args) {

	Comparator<String> cmplength = (String s1, String s2) -> Integer.compare(s1.length(),s2.length());          
	System.out.println(cmplength.compare("mario", "duke"));
					
	List<Person> actors = Arrays.asList(
			new Person("Brad","Pitt",48),
			new Person("John","Snow",27),
			new Person("Johnny","Depp",50),
			new Person("Jennifer","Aniston",50),
			new Person("jennifer","Lowrence",50),
			new Person("Melissa","Depp",50));
			
	System.out.println("\nactors:");
	actors.forEach(s -> System.out.println(s));
	
	
	/* Methode 1:
	  Comparator<Person> cmpBylastName = Comparator.comparing(Person::getLastName);	
	  Comparator<Person> cmplastNameThenfirstname =Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName);
	  Comparator<Person> cmpfirstnameThenLastname =Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName).reversed();	
	  
	  		
	actors.sort(cmpBylastName);
    System.out.println("\nvoici la comparaison des actors selon leur noms de famille:");
	actors.forEach(s -> System.out.println(s));

	actors.sort(cmplastNameThenfirstname);
    System.out.println("\nvoici la comparaison des actors selon leur noms de famille ensuite selon  leur prenoms :");
	actors.forEach(s -> System.out.println(s));
	
	actors.sort(cmpfirstnameThenLastname);
    System.out.println("\nvoici la comparaison  inverse des actors  :");
	actors.forEach(s -> System.out.println(s));
	*/
	
	Comparator<Person> cmpBylastName = Comparator.comparing(Person -> Person.getLastName());			
	Comparator<Person> cmpByFirstname = Comparator.comparing(Person -> Person.getFirstName());
	Comparator<Person> cmplastnamethenfirst = cmpBylastName.thenComparing(cmpByFirstname);
	Comparator<Person> reversed = cmplastnamethenfirst.reversed();
	
	List<Person> newactors=Arrays.asList(
			new Person("Brad","Pitt",48),
			new Person("John","Snow",27),
			null,
			new Person("Johnny","Depp",50)
			);
	
     Comparator<Person> cmpNull= Comparator.nullsLast(cmplastnamethenfirst);
    
    
    
	actors.sort(cmpBylastName);
	System.out.println("\ncompare by last name :");
	actors.forEach(s -> System.out.println(s));
	
	actors.sort(cmplastnamethenfirst);
	System.out.println("\ncompare by last name qnd then first name  :");
	actors.forEach(s -> System.out.println(s));
	
	actors.sort(reversed);
	System.out.println("\ncompare reversed :");
	actors.forEach(s -> System.out.println(s));

	newactors.sort(cmpNull);
	System.out.println("\navec une chaine null :");
    System.out.println(newactors.toString()+"\n");
	
	
	}

	
}
