package exo10;
import java.util.function.BiFunction;
import java.util.function.Function;
public class Exo10 {
	public static void main(String[] args) {
		
	
	Function<String, String> majscule =  s -> s.toUpperCase();
	System.out.println("cette cha�ne en majscule : " + majscule.apply("melissa"));
	
	Function<String, String> string =  s -> s ;
	System.out.println("voici la  cha�ne : " + string.apply("J'ai faim"));

	Function<String, Integer> longueur =  s -> s.length();
	System.out.println("longueur de cette cha�ne : " +longueur.apply("J'ai faim"));
	
	Function<String, String> parenthese =  s -> "("+s+")";
	System.out.println("cette cha�ne entre parenthese : " +parenthese.apply("love you dady"));
	
	BiFunction<String, String, Integer> indexOf =(String text, String word) -> text.indexOf(word);
	System.out.println("la position du mot dans la chaine  "+ indexOf.apply("momy love you too","you"));

   Function<String, Integer> chaine =txt -> indexOf.apply(txt, "abcdefghi");
   System.out.println("position d'une cha�ne dans une chaine " + chaine.apply(" alphabet: abcdefghi "));
			
			
		}

}
